var startTime = 0
var startTime1 = 0
var startTime2 = 0
var isClicked = false

var bannerStart = new TimelineMax({
  delay: 0,
  repeat: 0,
  repeatDelay: 0
})

var selectFirst = new TimelineMax({
  delay: 0,
  repeat: 0,
  repeatDelay: 0,
  paused: true
})

var selectSecond = new TimelineMax({
  delay: 0,
  repeat: 0,
  repeatDelay: 0,
  paused: true
})

bannerStart
  .set('.scene1', {
    opacity: 1,
    display: 'none'
  })
  .set('.answer1-block', {
    opacity: 0,
    display: 'none'
  })
  .set('.answer2-block', {
    opacity: 0,
    display: 'none'
  })
  .set('.text1', {
    yPercent: -200
  })
  .set('.inf', {
    yPercent: -300
  })
  .set('.button-block', {
    opacity: 1
  })
  .set('.comment-block', {
    opacity: 0
  })
  .set('.button2', {
    backgroundColor: 'inherit'
  })

  .to('.scene1', 0.1, {
    display: 'block'
  }, startTime)
  .to('.scene1', 0.1, {
    opacity: 1
  }, startTime += 0.1)
  .to('.text1', 0.4, {
    yPercent: 0,
    ease: Power2.easeOut,
    onComplete: function () {
      setTimeout(function () {
        if (!isClicked) {
          TweenMax.to('.button2', 0.4, {
            backgroundColor: '#009a3c',
            onComplete: function () {
              setTimeout(function () {
                selectSecond.restart()
              }, 1200)
            }
          })
        }
      }, 4000)
    }
  }, startTime += 0.2)

selectFirst
  .to('.answer1-block', 0.1, {
    display: 'block',
    opacity: 1
  }, startTime1)
  .to('.text1', 0.4, {
    yPercent: 200,
    ease: Power2.easeOut
  }, startTime1 += 0.1)
  .to('.button-block', 0.3, {
    opacity: 0
  }, startTime1 += 0.1)
  .to('.inf', 0.4, {
    yPercent: 0,
    ease: Power2.easeOut
  }, startTime1 += 0.2)
  .to('.comment-block', 0.4, {
    opacity: 1
  }, startTime1 += 0.2)

  .to('.scene1', 0.4, {
    opacity: 0
  }, startTime1 += 2)
  .to('.scene1', 0.4, {
    display: 'none'
  }, startTime1)
  .to('.scene2', 0.4, {
    display: 'block',
    opacity: 1
  }, startTime1 += 0.3)
  .to('.scene2', 0.4, {
    display: 'none',
    opacity: 0,
    onComplete: function () {
      startTime = 0
      startTime1 = 0
      startTime2 = 0
      bannerStart.restart()
    }
  }, startTime1 += 2)

selectSecond
  .to('.answer2-block', 0.1, {
    display: 'block',
    opacity: 1
  }, startTime2)
  .to('.text1', 0.4, {
    yPercent: 200,
    ease: Power2.easeOut
  }, startTime2 += 0.1)
  .to('.button-block', 0.3, {
    opacity: 0
  }, startTime2 += 0.1)
  .to('.inf', 0.4, {
    yPercent: 0,
    ease: Power2.easeOut
  }, startTime2 += 0.2)
  .to('.comment-block', 0.4, {
    opacity: 1
  }, startTime2 += 0.2)

  .to('.scene1', 0.4, {
    opacity: 0
  }, startTime2 += 2)
  .to('.scene1', 0.4, {
    display: 'none'
  }, startTime2)
  .to('.scene2', 0.4, {
    display: 'block',
    opacity: 1
  }, startTime2 += 0.3)
  .to('.scene2', 0.4, {
    display: 'none',
    opacity: 0,
    onComplete: function () {
      startTime = 0
      startTime1 = 0
      startTime2 = 0
      bannerStart.restart()
    }
  }, startTime2 += 2)

var button1 = document.getElementById('button1')
button1.addEventListener('click', function (e) {
  selectFirst.restart()
  isClicked = true
}, false)

var button2 = document.getElementById('button2')
button2.addEventListener('click', function (e) {
  selectSecond.restart()
  isClicked = true
}, false)
