faza = 0
schet = 0
schet2 = 0
schet3 = 0
spd = 8
spdOut = 1.2

isShowFirst = false
isShowFirst2 = false

needLineX = 0

_accelerationPoint = [0.3, 0.3, 0.3, 0.3, 0.3]
_accelerationAlpha = [0.01, 0.01, 0.01, 0.01, 0.01]

function positionElem (_name, _x, _y) {
  _name.style['transform'] = 'translate3d(' + _x + 'px, ' + _y + 'px, 0px)';
  _name.style['-o-transform'] = 'translate3d(' + _x + 'px, ' + _y + 'px, 0px)';
  _name.style['-ms-transform'] = 'translate3d(' + _x + 'px, ' + _y + 'px, 0px)';
  _name.style['-moz-transform'] = 'translate3d(' + _x + 'px, ' + _y + 'px, 0px)';
  _name.style['-webkit-transform'] = 'translate3d(' + _x + 'px, ' + _y + 'px, 0px)';
}

function showClip (_name, _needAlpha, _speed, _end, _needPause) {
  var _alpha = _name.style.opacity
  _alpha = parseFloat(_alpha)
  if (_alpha < _needAlpha - 0.05) {
    _alpha += (_needAlpha - _alpha) / _speed
  } else {
    _alpha = _needAlpha
  }
  _name.style.opacity = _alpha
  if (_alpha == _needAlpha && _end && _needPause) {
    schet++
    if (schet > _needPause) {
      anim = false
      nextFaza()
    }
  }
}

function hideClip (_name, _index, _needAlpha, _startSpeed, _speedAcceleration, _end, _needPause) {
  var _alpha = _name.style.opacity
  _alpha = parseFloat(_alpha)
  if (_alpha > _needAlpha) {
    _alpha -= _startSpeed + _accelerationAlpha[_index - 1]
    _accelerationAlpha[_index - 1] *= _speedAcceleration
  } else {
    _alpha = _needAlpha
  }
  _name.style.opacity = _alpha
  if (_alpha == _needAlpha) {
    if (_end && _needPause) {
      schet++
      if (schet > _needPause) {
        anim = false
        nextFaza()
      }
    }
  }
}

function moveToPoint (_name, _newX, _newY, _speed, _end, _needPause) {
  var _pointX
  var _pointY

  _pointX = _name.style['transform'].split('translate3d(')
  _pointX = parseFloat(_pointX[1])

  _pointY = _name.style['transform'].split('translate3d(')
  _pointY = _pointY[1]
  _pointY = _name.style['transform'].split('px,')
  _pointY = parseFloat(_pointY[1])

  if (_pointX < _newX - 0.1 || _pointX > _newX + 0.1) {
    _pointX += (_newX - _pointX) / _speed
  } else {
    _pointX = _newX
  }

  if (_pointY < _newY - 0.1 || _pointY > _newY + 0.1) {
    _pointY += (_newY - _pointY) / _speed
  } else {
    _pointY = _newY
  }

  positionElem(_name, _pointX, _pointY)

  if (_pointX == _newX && _pointY == _newY && _end && _needPause) {
    schet++
    if (schet > _needPause) {
      anim = false
      nextFaza()
    }
  }
}

function moveOutPoint (_name, _index1, _newY, _upToDown, _startSpeed, _speedAccelerationY) {
  var _pointX
  var _pointY

  _pointX = _name.style['transform'].split('translate3d(')
  _pointY = _pointX

  _pointX = parseFloat(_pointX[1])

  _pointY = _pointY[1].split('px, ')
  _pointY = parseFloat(_pointY[1])
  if (_upToDown) {
    if (_pointY < _newY) {
      _pointY += _startSpeed + _accelerationPoint[_index1 - 1]
    } else {
      _pointY = _newY
    }
  } else {
    if (_pointY > _newY) {
      _pointY -= _startSpeed + _accelerationPoint[_index1 - 1]
    } else {
      _pointY = _newY
    }
  }
  if (_pointY != _newY) {
    _accelerationPoint[_index1 - 1] *= _speedAccelerationY
  }
  positionElem(_name, _pointX, _pointY)
}

function nextFaza () {
  faza += 1
  anim = true
  schet = 1
  schet2 = 1
  _accelerationPoint = [0.3, 0.3, 0.3, 0.3, 0.3]
  _accelerationAlpha = [0.01, 0.01, 0.01, 0.01, 0.01]

}

var v2 = document.getElementById('v-2')
var v1file = document.getElementById('v-1-file')
var v2file = document.getElementById('v-2-file')
var video_black = document.getElementById('video_black')
var dragButton = document.getElementById('drag-button')
var mainTextLeft = document.getElementById('main-text-left')
var headerTop = document.getElementById('headerTop')
var text1Left = document.getElementById('text1-left')
var text1Right = document.getElementById('text1-right')
var text2Left = document.getElementById('text2-left')
var text2Right = document.getElementById('text2-right')
var mainBtnAnim = document.getElementById('mainBtnAnim')
var mainBtn = document.getElementById('mainBtn')
var btnIm1 = document.getElementById('btnIm1')
var btnIm2 = document.getElementById('btnIm2')
var bottomPl = document.getElementById('bottomPl')
var timerAutoplay = 1
var isDragging = false
var wasInteraction = false
var canDrag = false
var canPlayedVideo = true
var nowX = 0
var startedX = 0
var animText = false
var playVideos = true
var isLoaded = false

function isTouchDevice () {
  return 'ontouchstart' in window || navigator.maxTouchPoints
}

function getScreenWidth () {
  return Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
}

function getScreenHeight () {
  return Math.max(document.documentElement.clientHeight, window.innerHeight || 0)
}

function getSizes () {
  var w = getScreenWidth()
  var h = getScreenHeight()
  mainTextLeft.style.width = w + 'px'
  mainTextLeft.style.height = h + 'px'
  v1file.style.width = w + 'px'
  v2file.style.height = h + 'px'
  v2file.style.width = w + 'px'
  v2.style.width = w / 2 + 'px'
  v2.style.height = h + 'px'
  dragButton.style.left = w / 2 + 'px'
}
var firstDrag = true
var dragLeftRight = [true, true]
var lastX = 0

function startDrag (e) {
  if (firstDrag) {
    firstDrag = false
    activeVideo()
  }
  if (canDrag) {
    isDragging = true
    var x = isTouchDevice()
      ? e.touches[0].pageX
      : e.x
    startedX = x
    lastX = x
  }
}

function stopDrag (e) {
  if (canDrag && isDragging && !canPlayedVideo) {
    // canDrag = false
    var w = getScreenWidth()
    if (w / 2 - nowX > 0) {
      needLineX = -document.body.clientWidth / 2 - 200
    } else {
      needLineX = document.body.clientWidth / 2 + 200
    }
    // faza = 1;
  }
  isDragging = false
}

function dragging (e) {
  if (isDragging && canDrag) {
    var x = isTouchDevice()
      ? e.touches[0].pageX
      : e.x
    nowX = x
    if (lastX > x && dragLeftRight[0]) {
      dragLeftRight[0] = false
      moveSlider('left')
    }
    if (lastX < x && dragLeftRight[1]) {
      dragLeftRight[1] = false
      moveSlider('right')
    }
    if (startedX != x && canPlayedVideo) {
      canPlayedVideo = false
      animText = true
      resumeVideos()
      clearInterval(timerAutoplay)
    }
    if (x <= 0) {
      x = 0
    }
    if (x >= getScreenWidth() - 12) {
      x = getScreenWidth() - 12
    }
    dragButton.style.left = x + 'px'
    v2.style.width = x + 'px'
  }
}

getSizes()

positionElem(dragButton, -document.body.clientWidth / 1.5, 0)
positionElem(headerTop, 0, -document.body.clientWidth / 3)
positionElem(text1Left, -document.body.clientWidth / 2, 0)
positionElem(text1Right, document.body.clientWidth / 2, 0)
positionElem(text2Left, -document.body.clientWidth / 2, 0)
positionElem(text2Right, document.body.clientWidth / 2, 0)
positionElem(mainBtnAnim, document.body.clientWidth / 2, 0)

if (isTouchDevice()) {
  dragButton.addEventListener('touchstart', startDrag, false)
  document.addEventListener('touchend', stopDrag, false)
  document.addEventListener('touchmove', dragging, false)
} else {
  dragButton.addEventListener('mousedown', startDrag, false)
  document.addEventListener('mouseup', stopDrag, false)
  document.addEventListener('mousemove', dragging, false)
}
var chechVideoCvartal = [4, 3, 2, true, true, true]

function checkPlayback () {
  for (var i = 0; i < chechVideoCvartal.length / 2; i++) {
    if (v1file.currentTime > v1file.duration / chechVideoCvartal[i] && chechVideoCvartal[i + 3]) {
      chechVideoCvartal[i + 3] = false
      checkVideo(i + 1)
    }
  }
  if (v1file.currentTime > 0) {
    playVideos = false
  }
  if (v1file.currentTime >= 2.4 && v1file.currentTime <= 2.5) {
    isShowFirst2 = true
  }
  if (v1file.currentTime >= 2.6 && v1file.currentTime < 2.7) {
    isShowFirst = true
  }
  if (v1file.currentTime >= 3 && !wasInteraction) {
    timerAutoplay = setTimeout(function () {
      canDrag = false
      needLineX = -document.body.clientWidth / 2 - 300
      if (!allStop) {
        resumeVideos()
      }
      animText = true
      faza = 1
    }, 5000)
    wasInteraction = true
    stopedVideos()
    canDrag = true
  }
  if (v1file.currentTime >= v1file.duration - 0.1) {
    checkVideo(4)
    clearInterval(video1CheckTime)
    v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_FINISHED')
  }
}
timerAnim = 1
v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_MAINVIDEO_PAUSE')

function init () {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_MAINVIDEO_RESUME')
  isLoaded = true
  text1Left.style.opacity = 1
  text1Right.style.opacity = 1
  video_black.volume = 0
  v1file.volume = 0
  v1file.play()
  video_black.play()
  timerAnim = setInterval(moveVideoMask, 33)
}

function moveVideoMask () {
  if (faza <= 1) {
    var needW = parseFloat(dragButton.style.left.split('px')[0]) + parseFloat(dragButton.style['transform'].split('translate3d(')[1])
    if (needW <= 1) {
      v2.style.width = 1
      v2.style.display = 'none'
    } else {
      v2.style.width = needW + 'px'
      v2.style.display = 'block'
    }
  }
  if (isShowFirst2 && faza < 1) {
    moveToPoint(dragButton, 0, 0, 4, false)
  }
  if (isShowFirst && faza < 1) {
    moveToPoint(text1Left, 0, 0, 4, false)
    moveToPoint(text1Right, 0, 0, 4, false)
    moveToPoint(headerTop, 0, 0, 4, false)
  } else {
    if (faza >= 1) {
      // moveOutPoint(headerTop, 1, -document.body.clientWidth / 2, false, 1, spdOut)

    }
  }
  if (faza === 1) {
    moveToPoint(dragButton, needLineX, 0, 12, false)
    if (Math.abs(parseFloat(dragButton.style['transform'].split('translate3d(')[1])) > document.body.clientWidth / 2 + 200) {
      faza++
    }
  }
  if (animText) {
    isShowFirst = false
    schet2++
    moveToPoint(headerTop, 0, -document.body.clientWidth / 3, 12, false)
    hideClip(text1Right, 1, 0, 0, spdOut, false)
    hideClip(text1Left, 2, 0, 0, spdOut, false)
    if (schet2 > 15) {
      moveToPoint(text2Left, 0, 0, 6, false)
      moveToPoint(text2Right, 0, 0, 6, false)
    }
    if (schet2 > 25) {
      moveToPoint(mainBtnAnim, 0, 0, 6, false)
    }
    if (schet2 > 65) {
      animText = false
    }
  }
  if (faza == 2 && !animText) {
    clearInterval(timerAnim)
  }
}

mainBtn.addEventListener('click', clickBtnCta, false)
bottomPl.addEventListener('click', clickBtnBottom, false)
mainBtn.addEventListener('mouseover', function () {
  btnIm1.style.opacity = 0
  btnIm2.style.opacity = 1
}, false)
mainBtn.addEventListener('mouseleave', function () {
  btnIm1.style.opacity = 1
  btnIm2.style.opacity = 0
}, false)
// v2file.addEventListener('playing', checkPlayback, false)
// v1file.addEventListener('timeupdate', checkPlayback, false)

video1CheckTime = setInterval(checkPlayback, 33)

window.addEventListener('resize', getSizes, false)

window.addEventListener('load', init, false)
window.addEventListener('mousemove', function () {
  if (playVideos && isLoaded) {
    playVideos = false
    resumeVideos()
  }
}, false)

function stopedVideos () {
  v1file.pause()
  video_black.pause()
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_MAINVIDEO_PAUSE')
}

function resumeVideos () {
  v1file.play()
  video_black.play()
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_MAINVIDEO_RESUME')
}
