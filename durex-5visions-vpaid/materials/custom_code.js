// custom параметры
isTest = true
allStop = false
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_CONFIG_PARAMS', function (params) {
  customParams = params
})

// ссылка с основного видео
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_CONFIG_MAINVIDEOCLICKTHRU', function (params) {
  wrapper.addEventListener('click', function () {
    switch (state) {
      case 'none':
        window.open(params.url)
        v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
          event: EVENT_ACTIVE,
          params: {
            answers: 'MainVideo_Click'
          }
        })
        v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_CLICKTHRU')
        break
      case 'intro':
        v5('#final').style.display = 'flex'
        close.style.display = 'block'
        v5('#warnTxt').style.display = 'block'
        state = 'info'
        tml.play()
        v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT_TRACKPIXEL', {
          event: 'Pixel_Nakopitelnyi'
        })
        v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT_TRACKPIXEL', {
          event: 'Pixel_Otricatelnyi'
        })
        break
      case 'info':
        break
    }
  })
})

// плеер встал на паузу
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_PAUSE', function () {
  allStop = true
  stopedVideos()
})

// плеер продолжил воспроизведение
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_RESUME', function () {
  allStop = false
  resumeVideos()
})

// плеер завершил воспроизведение креатив
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_STOP', function () {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_STOP__OK')
})

// плеер начал воспроизведение креатива
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_START', function () {})

// изменился уровень звука у плеера
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_VOLUME', function (params) {})

// таймер креатива
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_TIME', function (params) {})

// таймер основного видео
v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_MAINVIDEO_TIME', function (params) {})
// v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_MAINVIDEO_PAUSE');
/**
 * Инициализация и загрузка ресурсов завершена
 * Сообщение фреймворку, что креатив готов к началу воспроизведения
 */

var customParams

v5ViPlusCustomProxy.addEventListener('VP_CUSTOM_IFRAME_READY', function (params) {})
v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_READY')

function clickBtnCta () {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Click_CTA'
    }
  })
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Click_CTA2'
    }
  })
  window.open(customParams.Click_CTA)
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_CLICKTHRU')
}

function clickBtnBottom () {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Click_Bottom'
    }
  })
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Click_Bottom2'
    }
  })
  window.open(customParams.Click_Bottom)
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_CLICKTHRU')
}

function activeVideo () {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Activate_slider'
    }
  })
}

function moveSlider (_side) {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Pull_slider_' + _side
    }
  })
}

function checkVideo (_kvartal) {
  v5ViPlusCustomProxy.dispatchEvent('VP_CUSTOM_IFRAME_VIPLUSEVENT', {
    event: EVENT_ACTIVE,
    params: {
      answers: 'Watch_video_' + 25 * _kvartal + '%'
    }
  })
}
