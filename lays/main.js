// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel

// MIT license

(function () {
  var lastTime = 0
  var vendors = ['ms', 'moz', 'webkit', 'o']
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame']
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame']
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback, element) {
      var currTime = new Date().getTime()
      var timeToCall = Math.max(0, 16 - (currTime - lastTime))
      var id = window.setTimeout(function () {
        callback(currTime + timeToCall)
      }, timeToCall)
      lastTime = currTime + timeToCall
      return id
    }
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id)
    }
  }
})()

window.mobileAndTabletCheck = function () {
  var check = false;
  (function (a) {
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
      check = true
    }
  })(navigator.userAgent || navigator.vendor || window.opera)
  return check
}

// Extend Math
Math.radians = function (degrees) {
  return degrees * Math.PI / 180
}
Math.degrees = function (radians) {
  return radians * 180 / Math.PI
}

function getOrientation () {
  if (window.innerHeight > window.innerWidth) {
    return 'portrait'
  } else {
    return 'landscape'
  }
}

// Leonid's arc angles function
var p1 = 0
var spdP1 = Math.PI / 50
var angles = [0, 0]

function reAngles () {
  p1 = 0
  spdP1 = Math.PI / 50
  angles = [0, 0]
}

function newAngles () {
  mnP1 = 0
  var p2 = p1
  while (p2 > Math.PI / 2) {
    mnP1++
    p2 -= Math.PI / 2
  }
  if (mnP1 == 0) {
    angles[0] = Math.sin(p1) * 90
    angles[1] = 0
  }
  if (mnP1 == 1 || mnP1 == 2) {
    angles[0] = 90
    angles[1] = Math.sin(p1 - Math.PI / 2) * 90
  }
  if (mnP1 == 3) {
    angles[0] = Math.sin(p1) * -90
    angles[1] = 0
  }
  // console.log(Math.round(angles[0]) + " " + Math.round(angles[1]) + " " + mnP1);
  p1 += spdP1
  if (p1 > Math.PI * 2) {
    p1 -= Math.PI * 2
  }
}

var bannerContainer = document.getElementById('banner-container')
var orientationWarning = document.getElementById('orientation-warning')

function showBanner () {
  orientationWarning.style.display = 'none'

  setTimeout(function () {
    bannerContainer.style.display = 'block'
  }, 400)
}

function showWarning () {
  orientationWarning.style.display = 'block'

  setTimeout(function () {
    bannerContainer.style.display = 'none'
  }, 100)
}

function checkOrientation () {
  if (getOrientation() === 'landscape') {
    showBanner()
  } else {
    showWarning()
  }
}

checkOrientation()

window.addEventListener('resize', function () {
  checkOrientation()
  setFullscreenCanvas()
}, false)

window.addEventListener('load', function () {}, false)

var c = document.getElementById('game-canvas')
var ctx = c.getContext('2d')

setFullscreenCanvas()

var cw
var ch

function setFullscreenCanvas () {
  c.width = window.innerWidth
  c.height = window.innerHeight

  cw = c.width
  ch = c.height
}

var x = 0
var panelColor = '#fc4a00'

var playButton = document.getElementById('button-play')
playButton.addEventListener('click', function (e) {
  // console.log(event.pageX, event.pageY);
  game.gsap.firstPlay()
  parent.postMessage('banner.event.1', '*')
}, false)

var replayOneButton = document.getElementById('button-replay-one')
replayOneButton.addEventListener('click', function (e) {
  // console.log(event.pageX, event.pageY);
  game.gsap.firstPlay()
  parent.postMessage('banner.event.2', '*')
}, false)

var replayButton = document.getElementById('button-replay')
replayButton.addEventListener('click', function (e) {
  // console.log(event.pageX, event.pageY);
  game.gsap.firstPlay()
  parent.postMessage('banner.event.3', '*')
}, false)

var knowmoreButton = document.getElementById('button-knowmore')
knowmoreButton.addEventListener('click', function (e) {
  parent.postMessage('banner.redirect', '*')
  parent.postMessage('banner.event.4', '*')
}, false)

var handContainer = document.getElementById('hand-container')

var game = {
  init: function () {
    this.gsap.showStart()
  },
  states: {
    isGameOver: false,
    isThrow: false,
    isCanThrow: true,
    killed: {
      green: false,
      red: false,
      blue: false
    }
  },
  ammo: 6,
  hits: 0,
  hitedPacks: 0,
  coords: {
    x: 0,
    y: 0,
    handX: function () {
      return handContainer.offsetLeft
    },
    handY: function () {
      return handContainer.offsetTop
    },
    crosshair: {
      x: window.innerWidth / 2,
      y: window.innerHeight / 2
    },
    target: {
      x: function (id) {
        var t = document.getElementById(id)
        return t.getBoundingClientRect().left
      },
      y: function (id) {
        var t = document.getElementById(id)
        return t.getBoundingClientRect().top
      },
      w: function (id) {
        var t = document.getElementById(id)
        return t.getBoundingClientRect().width
      },
      h: function (id) {
        var t = document.getElementById(id)
        return t.getBoundingClientRect().height
      }
    }
  },
  loadImages: function () {
    var girlNormal = new Image()
    girlNormal.src = 'girl-normal.png'

    var girlSmile = new Image()
    girlSmile.src = 'girl-smile.png'

    var girlSad = new Image()
    girlSad.src = 'girl-sad.png'

    this.images = {
      girlNormal: girlNormal,
      girlSmile: girlSmile,
      girlSad: girlSad
    }
  },
  images: {},
  strength: {
    counter: 0
  },
  draw: function () {
    ctx.clearRect(0, 0, cw, ch)

    var dS = (game.coords.handX() - game.coords.crosshair.x) / 2
    var ellipseX = game.coords.handX() - dS

    var pointDx = window.innerWidth * 0.05
    var pointDy = window.innerHeight * 0.07

    function drawArc1 (lineWidth, color, lineDash, modifer) {
      ctx.beginPath()
      ctx.setLineDash(lineDash)
      ctx.lineWidth = lineWidth
      ctx.strokeStyle = color

      var x = ellipseX
      var y = game.coords.handY() + pointDy
      var rx = Math.abs(dS)
      var ry = game.coords.handY() - window.innerHeight * 0.35 + pointDy
      var rotation = 0
      var start = 0 // The start angle (in radians)
      var end = 2 * Math.PI // The end angle (in radians)
      var anticlockwise = true

      if (dS > 0) {
        if (modifer !== 'd') {
          ctx.ellipse(x, y, rx, ry, rotation, 0, -Math.PI * 0.5 * (modifer / 90), true)
        } else {
          ctx.ellipse(x, y, rx, ry, rotation, 0, -Math.PI * 0.5 * 1, true)
        }
      } else {
        if (modifer !== 'd') {
          ctx.ellipse(x, y, rx, ry, rotation, Math.PI, Math.PI * (1 + 0.5 * (modifer / 90)), false)
        } else {
          ctx.ellipse(x, y, rx, ry, rotation, Math.PI, Math.PI * (1 + 0.5), false)
        }
      }

      ctx.stroke()
    }

    function drawArc2 (lineWidth, color, lineDash, modifer) {
      ctx.beginPath()
      ctx.setLineDash(lineDash)
      ctx.lineWidth = lineWidth
      ctx.strokeStyle = color

      var x = ellipseX
      var y = game.coords.crosshair.y
      var rx = Math.abs(dS)
      var ry = game.coords.crosshair.y - window.innerHeight * 0.35
      var rotation = 0
      var start = 0 // The start angle (in radians)
      var end = 2 * Math.PI // The end angle (in radians)
      var anticlockwise = true

      if (dS > 0) {
        if (modifer !== 'd') {
          ctx.ellipse(x, y, rx, ry, rotation, 1.5 * Math.PI, Math.PI * (1.5 - 0.5 * (modifer / 90)), true)
        } else {
          ctx.ellipse(x, y, rx, ry, rotation, 1.5 * Math.PI, Math.PI * 1, true)
        }
      } else {
        if (modifer !== 'd') {
          ctx.ellipse(x, y, rx, ry, rotation, -0.5 * Math.PI, -Math.PI * (2.5 - 0.5 * (modifer / 90)), false)
        } else {
          ctx.ellipse(x, y, rx, ry, rotation, -0.5 * Math.PI, -Math.PI * 2, false)
        }
      }

      ctx.stroke()
    }

    drawArc1(4, '#ffffff', [
      10, 15
    ], 'd')
    drawArc2(4, '#ffffff', [
      10, 15
    ], 'd')

    drawArc1(8, '#000000', [], angles[0])
    drawArc1(6, '#fc4a00', [], angles[0])
    drawArc2(8, '#000000', [], angles[1])
    drawArc2(6, '#fc4a00', [], angles[1])

    x++
    game.strength.counter += 2

    // console.log(game.coords.crosshair.x, game.coords.crosshair.y)

    newAngles()
  },
  animate: function () {
    game.play = requestAnimationFrame(game.animate)
    game.draw()
  },
  gsap: {
    initPositions: function () {
      // ---
    },
    showStart: function () {
      var t = new TimelineMax({
        delay: 0,
        repeat: 0,
        repeatDelay: 0
      })
      var startTime = 0

      t.set('.girl-start', {
        xPercent: -150
      })
      t.set('.bubble-start', {
        scale: 0,
        opacity: 0
      })

      TweenMax.set('.br1', {
        opacity: 0,
        display: 'none'
      })

      TweenMax.set('.br2', {
        opacity: 0,
        display: 'none'
      })

      TweenMax.set([
        '.gb1',
        '.gb2',
        '.gb3',
        '.gb4',
        '.gb5',
        '.gb6'
      ], {
        opacity: 0,
        display: 'none'
      })

      t.set('.girl-lose', {
        opacity: 0
      })
      t.set('.bubble-lose', {
        scale: 0,
        opacity: 0
      })

      t.set('.girl-win', {
        opacity: 0
      })
      t.set('.bubble-win', {
        scale: 0,
        opacity: 0
      })

      t.set('.crosshair', {
        opacity: 0,
        display: 'none'
      })

      t.set('.button-play', {
        yPercent: 150
      })
      t.set('.button-replay-one', {
        yPercent: 150
      })
      t.set('.button-replay', {
        yPercent: 150
      })
      t.set('.button-knowmore', {
        yPercent: 150
      })

      t.set('.bullets-text', {
        opacity: 0
      })
      t.set('.bullets-peppers', {
        yPercent: 300
      })

      t.set('.target-green', {
        xPercent: 500
      })
      t.set('.target-red', {
        xPercent: 500
      })
      t.set('.target-blue', {
        xPercent: 500
      })

      t.set('.hand-container', {
        opacity: 0,
        display: 'none'
      })

      t.to('.girl-start', 0.4, {
        xPercent: 0,
        ease: Power1.easeOut
      }, startTime)
      t.to('.bubble-start', 0.55, {
        scale: 1,
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.4))
      t.to('.button-play', 0.4, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.4))
    },
    firstPlay: function () {
      game.states = {
        isGameOver: false,
        isThrow: false,
        isCanThrow: true,
        killed: {
          green: false,
          red: false,
          blue: false
        }
      }
      game.ammo = 6
      game.hits = 0
      game.hitedPacks = 0

      cancelAnimationFrame(game.play)
      reAngles()

      c.removeEventListener('click', function (e) {}, false)
      document.removeEventListener('mousemove', function (e) {}, false)
      document.removeEventListener('touchmove', function (e) {}, false)
      document.removeEventListener('mouseup', function (e) {}, false)
      document.removeEventListener('touchend', function (e) {}, false)

      game.states.isGameOver = false
      game.states.isGreen = false
      game.states.isRed = false
      game.states.isBlue = false

      TweenMax.set('#t-green', {
        opacity: 1
      })
      TweenMax.set('#t-red', {
        opacity: 1
      })
      TweenMax.set('#t-blue', {
        opacity: 1
      })

      TweenMax.set('.big-packs', {
        opacity: 0,
        display: 'none'
      })
      TweenMax.set('.big-pack-green', {
        opacity: 0,
        scale: 0
      })
      TweenMax.set('.big-pack-red', {
        opacity: 0,
        scale: 0
      })
      TweenMax.set('.big-pack-blue', {
        opacity: 0,
        scale: 0
      })

      TweenMax.set('.br1', {
        opacity: 0,
        display: 'none'
      })

      TweenMax.set('.br2', {
        opacity: 0,
        display: 'none'
      })

      TweenMax.set([
        '.gb1',
        '.gb2',
        '.gb3',
        '.gb4',
        '.gb5',
        '.gb6'
      ], {
        opacity: 0,
        display: 'none'
      })

      TweenMax.set('#counter-0', {
        display: 'block'
      })
      TweenMax.set('#counter-1', {
        display: 'none'
      })
      TweenMax.set('#counter-2', {
        display: 'none'
      })
      TweenMax.set('#counter-3', {
        display: 'none'
      })

      game.states.isGreenShowed = false
      game.states.isRedShowed = false
      game.states.isBlueShowed = false

      TweenMax.set([
        '.game-canvas', '.crosshair', '.hw', '.bullets'
      ], {
        opacity: 1,
        display: 'block'
      })

      var select = document.getElementById('bullets-peppers')
      for (var i = 0; i < select.children.length; i++) {
        select.children[i].style.opacity = 1
      }

      TweenMax.set([
        '.game-canvas', '.crosshair'
      ], {
        opacity: 1
      })

      var t = new TimelineMax({
        delay: 0,
        repeat: 0,
        repeatDelay: 0
      })
      var startTime = 0

      t.set('.girl-lose', {
        opacity: 0
      })
      t.set('.bubble-lose', {
        scale: 0,
        opacity: 0
      })
      t.set('.button-replay-one', {
        yPercent: 150
      })

      t.set('.crosshair', {
        display: 'block'
      })

      t.to([
        '.button-play', '.button-knowmore', '.button-replay', '.button-replay-one'
      ], 0.3, {
        yPercent: 150,
        ease: Power1.easeOut
      }, startTime)
      t.to('.small-packs', 0.4, {
        xPercent: 150,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to([
        '.bubble-start', '.bubble-lose', '.bubble-win'
      ], 0.3, {
        scale: 0,
        opacity: 0,
        ease: Power1.easeOut,
        onComplete: function () {
          var t = new TimelineMax({
            delay: 0,
            repeat: 0,
            repeatDelay: 0
          })
          var startTime = 0

          t.to('.br1', 0.2, {
            opacity: 1,
            display: 'block'
          }, (startTime))
          t.to('.br2', 0.2, {
            opacity: 1,
            display: 'block'
          }, (startTime += 4))

          t.to([
            '.br1', '.br2'
          ], 0.2, {
            opacity: 0,
            display: 'none'
          }, (startTime += 4))
        }
      }, (startTime += 0.2))

      t.to('.bullets-text', 0.3, {
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.bullets-peppers', 0.3, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))

      t.to('.target-green', 0.6, {
        xPercent: 0,
        ease: Power2.easeOut
      }, (startTime += 0.2))
      t.to('.target-red', 0.6, {
        xPercent: 0,
        ease: Power2.easeOut
      }, (startTime += 0.2))
      t.to('.target-blue', 0.6, {
        xPercent: 0,
        ease: Power2.easeOut,
        onComplete: function () {
          t.set('.hand-container', {
            opacity: 1,
            display: 'block'
          })

          t.set('.crosshair', {
            opacity: 1
          })

          TweenMax.to([
            '.game-canvas', '.crosshair', '.hw'
          ], 0.1, {
            opacity: 1,
            visibility: 'visible'
          })

          game.animate()
        }
      }, (startTime += 0.2))

      var crosshair = document.getElementById('crosshair')

      var crosshairW = crosshair.clientWidth
      var crosshairH = crosshair.clientHeight

      game.coords.crosshair.x = crosshair.offsetLeft + crosshairW / 2
      game.coords.crosshair.y = crosshair.offsetTop + crosshairH / 2

      c.addEventListener('click', function (e) {
        // console.log(event.pageX, event.pageY);
      }, false)

      if (window.mobileAndTabletCheck()) {
        document.addEventListener('touchend', function (e) {
          if (game.states.isCanThrow) {
            game.gsap.drawPepper()
          }
        }, false)

        document.addEventListener('touchmove', function (e) {
          // e.preventDefault();
          // console.log(e);
          var mX = e.touches[0].pageX
          var mY = e.touches[0].pageY

          game.coords.x = mX
          game.coords.y = mY

          if (mY < window.innerHeight * 0.9 && mY > window.innerHeight * 0.35) {
            crosshair.style.top = mY - crosshairH / 2 + 'px'
            crosshair.style.left = mX - crosshairW / 2 + 'px'
          }

          game.coords.crosshair.x = crosshair.offsetLeft + crosshairW / 2
          game.coords.crosshair.y = crosshair.offsetTop + crosshairH / 2
        }, false)
      } else {
        document.addEventListener('mouseup', function (e) {
          console.log('mouseup')
          if (game.states.isCanThrow) {
            game.gsap.drawPepper()
          }
        }, false)

        document.addEventListener('mousemove', function (e) {
          var mX = event.pageX
          var mY = event.pageY

          game.coords.x = mX
          game.coords.y = mY

          if (mY < window.innerHeight * 0.9 && mY > window.innerHeight * 0.35) {
            crosshair.style.top = mY - crosshairH / 2 + 'px'
            crosshair.style.left = mX - crosshairW / 2 + 'px'
          }

          game.coords.crosshair.x = crosshair.offsetLeft + crosshairW / 2
          game.coords.crosshair.y = crosshair.offsetTop + crosshairH / 2
        }, false)
      }
    },
    drawPepper: function () {
      console.log('drawPepper')

      function checkHit (id) {
        if (clickX > game.coords.target.x(id) && clickX < game.coords.target.x(id) + game.coords.target.w(id) * 0.78 && clickY > game.coords.target.y(id) && clickY < game.coords.target.y(id) + game.coords.target.h(id)) {
          if (angles[1] > 65) {
            console.info(id + ' hit')
            if (id === 't-green') {
              game.states.isGreen = true
            }

            if (id === 't-red') {
              game.states.isRed = true
            }

            if (id === 't-blue') {
              game.states.isBlue = true
            }
          }
        }
      }

      var clickX = game.coords.x
      var clickY = game.coords.y

      checkHit('t-green')
      checkHit('t-red')
      checkHit('t-blue')

      game.states.isCanThrow = false

      if (game.coords.y < window.innerHeight * 0.9 && game.coords.y > window.innerHeight * 0.35) {
        console.log('зашло')
        TweenMax.set([
          '.game-canvas', '.crosshair'
        ], {
          opacity: 0
        })

        if (angles[1] > 65) {
          console.log('max')
        } else {
          console.log('min')
        }

        game.hits += 1
        game.ammo -= 1
        console.log('ammo ' + game.ammo)
        console.log('hits ' + game.hits)

        var select = document.getElementById('bullets-peppers')
        if (game.ammo > 0) {
          select.children[game.ammo - 1].style.opacity = 0
        }
        // Rotate hand
        var t = new TimelineMax({
          delay: 0,
          repeat: 0,
          repeatDelay: 0
        })
        var startTime = 0

        var t = new TimelineMax({
          delay: 0,
          repeat: 0,
          repeatDelay: 0
        })
        var startTime = 0

        var bigPepper = document.getElementById('big-pepper')
        var bpX = bigPepper.getBoundingClientRect().left
        var bpY = bigPepper.getBoundingClientRect().top
        var bpW = bigPepper.getBoundingClientRect().width
        var bpH = bigPepper.getBoundingClientRect().height

        var dX = bpX + bpW / 2 - game.coords.x
        var dY = bpY + bpH / 2 - game.coords.y

        t.to('.hw', 0.2, {
          yPercent: 10,
          rotation: 60,
          transformOrigin: 'center 90%'
        }, startTime)
        t.to('.hw', 0.1, {
          yPercent: 0,
          rotation: 0,
          transformOrigin: 'center 90%'
        }, startTime += 0.2)

        if (angles[1] > 65 && (game.states.isGreen || game.states.isRed || game.states.isBlue)) {
          t.to('.big-pepper', 0.4, {
            // x: -dX,
            // y: -dY,
            bezier: {
              values: [{
                x: 0,
                y: 0
              }, {
                x: -dX / 2,
                y: -dY * 3.2
              }, {
                x: -dX,
                y: -dY
              }]
            },
            scale: 0.5,
            onComplete: function () {
              TweenMax.to('.big-pepper', 0.3, {
                opacity: 0
              })
              if (game.states.isGreen && !game.states.isGreenShowed) {
                TweenMax.to('#t-green', 0.3, {
                  opacity: 0
                })

                var t = new TimelineMax({
                  delay: 0,
                  repeat: 0,
                  repeatDelay: 0
                })
                var startTime = 0

                t.to([
                  '.game-canvas', '.crosshair', '.hw'
                ], 0.1, {
                  opacity: 0,
                  visibility: 'hidden'
                }, startTime)

                t.to('.big-packs', 0.3, {
                  opacity: 1,
                  display: 'block'
                }, (startTime += 0.1))

                t.to('.big-pack-green', 0.4, {
                  scale: 1,
                  opacity: 1
                }, (startTime += 0.2))

                t.to('.big-packs', 0.3, {
                  opacity: 0,
                  display: 'none'
                }, (startTime += 1.5))

                t.to('.big-pack-green', 0.1, {
                  scale: 0,
                  opacity: 0,
                  onComplete: function () {
                    game.states.killed.green = true

                    game.states.isGreenShowed = true
                    game.hitedPacks += 1

                    var counterId = '#counter-' + game.hitedPacks
                    TweenMax.set(counterId, {
                      display: 'block'
                    })

                    if (game.hitedPacks === 3) {
                      console.log('win')
                      game.gsap.showWin()
                    } else {
                      TweenMax.to([
                        '.game-canvas', '.crosshair', '.hw'
                      ], 0.1, {
                        opacity: 1,
                        visibility: 'visible'
                      })

                      TweenMax.delayedCall(2, function () {
                        game.states.isCanThrow = true
                      })
                    }
                  }
                }, (startTime += 0.1))
              }

              if (game.states.isRed && !game.states.isRedShowed) {
                TweenMax.to('#t-red', 0.3, {
                  opacity: 0
                })

                var t = new TimelineMax({
                  delay: 0,
                  repeat: 0,
                  repeatDelay: 0
                })
                var startTime = 0

                t.to([
                  '.game-canvas', '.crosshair', '.hw'
                ], 0.1, {
                  opacity: 0,
                  visibility: 'hidden'
                }, startTime)

                t.to('.big-packs', 0.3, {
                  opacity: 1,
                  display: 'block'
                }, (startTime += 0.1))

                t.to('.big-pack-red', 0.4, {
                  scale: 1,
                  opacity: 1
                }, (startTime += 0.2))

                t.to('.big-packs', 0.3, {
                  opacity: 0,
                  display: 'none'
                }, (startTime += 1.5))

                t.to('.big-pack-red', 0.1, {
                  scale: 0,
                  opacity: 0,
                  onComplete: function () {
                    game.states.killed.red = true

                    game.states.isRedShowed = true
                    game.hitedPacks += 1

                    var counterId = '#counter-' + game.hitedPacks
                    TweenMax.set(counterId, {
                      display: 'block'
                    })

                    if (game.hitedPacks === 3) {
                      console.log('win')
                      game.gsap.showWin()
                    } else {
                      TweenMax.to([
                        '.game-canvas', '.crosshair', '.hw'
                      ], 0.1, {
                        opacity: 1,
                        visibility: 'visible'
                      })

                      TweenMax.delayedCall(2, function () {
                        game.states.isCanThrow = true
                      })
                    }
                  }
                }, startTime += 0.1)
              }

              if (game.states.isBlue && !game.states.isBlueShowed) {
                TweenMax.to('#t-blue', 0.3, {
                  opacity: 0
                })

                var t = new TimelineMax({
                  delay: 0,
                  repeat: 0,
                  repeatDelay: 0
                })
                var startTime = 0

                t.to([
                  '.game-canvas', '.crosshair', '.hw'
                ], 0.1, {
                  opacity: 0,
                  visibility: 'hidden'
                }, startTime)

                t.to('.big-packs', 0.3, {
                  opacity: 1,
                  display: 'block'
                }, (startTime += 0.1))

                t.to('.big-pack-blue', 0.4, {
                  scale: 1,
                  opacity: 1
                }, (startTime += 0.2))

                t.to('.big-packs', 0.3, {
                  opacity: 0,
                  display: 'none'
                }, (startTime += 1.5))

                t.to('.big-pack-blue', 0.1, {
                  scale: 0,
                  opacity: 0,
                  onComplete: function () {
                    game.states.killed.blue = true

                    game.states.isBlueShowed = true
                    game.hitedPacks += 1

                    var counterId = '#counter-' + game.hitedPacks
                    TweenMax.set(counterId, {
                      display: 'block'
                    })

                    if (game.hitedPacks === 3) {
                      console.log('win')
                      game.gsap.showWin()
                    } else {
                      TweenMax.to([
                        '.game-canvas', '.crosshair', '.hw'
                      ], 0.1, {
                        opacity: 1,
                        visibility: 'visible'
                      })

                      TweenMax.delayedCall(2, function () {
                        game.states.isCanThrow = true
                      })
                    }
                  }
                }, startTime += 0.1)
              }

              if (game.states.isGreenShowed || game.states.isRedShowed || game.states.isBlueShowed) {
                TweenMax.delayedCall(2, function () {
                  game.states.isCanThrow = true
                })
              }

              // game.states.isCanThrow = true;
            }
          }, startTime += 0)
        } else {
          t.to('.big-pepper', 0.4, {
            // x: -dX,
            // y: -dY,
            bezier: {
              values: [{
                x: 0,
                y: 0
              }, {
                x: -dX / 2,
                y: -dY * 3.2
              }, {
                x: -dX,
                y: -dY
              }]
            },
            scale: 0.7,
            onComplete: function () {
              TweenMax.to('.big-pepper', 0.8, {
                y: 800,
                opacity: 0
              })
              console.log('мимо')
              game.states.isCanThrow = true
            }
          }, startTime += 0)
        }

        t.to([
          '.hand-full', '.hand-top'
        ], 0.2, {
          yPercent: 0,
          rotation: -30,
          transformOrigin: 'center 90%',
          onComplete: function () {}
        }, startTime)
        t.to([
          '.hand-full', '.hand-top'
        ], 0.1, {
          yPercent: 0,
          rotation: 0,
          transformOrigin: 'center 90%',
          onComplete: function () {
            if (game.ammo > 0) {
              // select.removeChild(select.lastElementChild);
              // select.children[game.ammo - 1].style.opacity = 0;
            } else {
              game.states.isGameOver = true
              if (game.hits === 6 && game.ammo === 0) {
                console.log('no ammo')
                game.gsap.showLose()
              }
            }

            TweenMax.set('.big-pepper', {
              x: 0,
              y: 0,
              scale: 1,
              opacity: 1
            })

            if (!game.states.isGameOver) {
              TweenMax.set([
                '.game-canvas', '.crosshair'
              ], {
                opacity: 1
              })
            } else {
              TweenMax.set(['.hand-container'], {
                opacity: 0,
                display: 'block'
              })

              TweenMax.to([
                '.game-canvas', '.crosshair', '.hw'
              ], 0.1, {
                opacity: 0,
                visibility: 'hidden'
              })
            }
          }
        }, startTime += 0.5)
      } else {
        console.log(game.states.isCanThrow)
        game.states.isCanThrow = true
      }
    },
    showLose: function () {
      var bubbleWinClass = '.bubble-win' + game.gsap.chooseFinalBubble()
      console.log(bubbleWinClass)

      var t = new TimelineMax({
        delay: 0,
        repeat: 0,
        repeatDelay: 0
      })
      var startTime = 0

      cancelAnimationFrame(game.play)

      TweenMax.set('#counter-0', {
        display: 'none'
      })
      TweenMax.set('.bubble-lose', {
        opacity: 0
      })

      t.to([
        '.game-canvas', '.crosshair', '.hw', '.bullets'
      ], 0.1, {
        opacity: 0,
        display: 'none'
      }, startTime)

      t.to('.girl-win', 0.01, {
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.small-packs', 0.4, {
        xPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to(bubbleWinClass, 0.55, {
        scale: 1,
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.button-replay', 0.3, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.button-knowmore', 0.3, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
    },
    showWin: function () {
      var bubbleWinClass = '.bubble-win' + game.gsap.chooseFinalBubble()
      console.log(bubbleWinClass)

      var t = new TimelineMax({
        delay: 0,
        repeat: 0,
        repeatDelay: 0
      })
      var startTime = 0

      cancelAnimationFrame(game.play)

      TweenMax.set('#counter-0', {
        display: 'none'
      })
      TweenMax.set('.bubble-lose', {
        opacity: 0
      })

      t.to([
        '.game-canvas', '.crosshair', '.hw', '.bullets'
      ], 0.1, {
        opacity: 0,
        display: 'none'
      }, startTime)

      t.to('.girl-win', 0.01, {
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.small-packs', 0.4, {
        xPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to(bubbleWinClass, 0.55, {
        scale: 1,
        opacity: 1,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.button-replay', 0.3, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
      t.to('.button-knowmore', 0.3, {
        yPercent: 0,
        ease: Power1.easeOut
      }, (startTime += 0.2))
    },
    chooseFinalBubble: function () {
      console.log(game.states.killed)
      var gb = '.fb0'
      var k = game.states.killed

      if (k.green === false && k.red === true && k.blue === false) {
        gb = '.fb1'
      }

      if (k.green === false && k.red === true && k.blue === true) {
        gb = '.fb2'
      }

      if (k.green === true && k.red === false && k.blue === true) {
        gb = '.fb3'
      }

      if (k.green === true && k.red === false && k.blue === false) {
        gb = '.fb4'
      }

      if (k.green === false && k.red === false && k.blue === true) {
        gb = '.fb5'
      }

      if (k.green === false && k.red === false && k.blue === false) {
        gb = '.fb6'
      }

      if (k.green === true && k.red === true && k.blue === false) {
        gb = '.fb7'
      }

      return gb
    }
  }
}

game.init()
