console.log('Start...')

var banner = document.getElementById('banner')
var scene1 = document.getElementById('scene1')
var scene2 = document.getElementById('scene2')
var scene3 = document.getElementById('scene3')

var photo = document.getElementById('photo')
var photoFader = document.getElementById('photo-fader')
var photoFlash = document.getElementById('photo-flash')
var crosshair = document.getElementById('crosshair')
var message0 = document.getElementById('message0')
var message1 = document.getElementById('message1')
var message2 = document.getElementById('message2')
var message3 = document.getElementById('message3')
var whiteBlock = document.getElementById('white-button-block')
var shotButton = document.getElementById('shot-button')
var messageBlock = document.getElementById('messages-block')
var packshotText = document.getElementById('packshot-text')
var btnAppstore = document.getElementById('button-appstore')
var btnGoogleplay = document.getElementById('button-googleplay')
var numbers = document.getElementById('numbers')

var startTime = 0

function applyBlur () {
  TweenMax.set(photo, {
    webkitFilter: 'blur(' + blurElement.a + 'px)',
    filter: 'blur(' + blurElement.a + 'px)'
  })
}

var tl_P = new TimelineMax({
  delay: 0,
  repeat: 0,
  repeatDelay: 0
})

var tl_L = new TimelineMax({
  delay: 0,
  repeat: 0,
  repeatDelay: 0
})

var blurElement = {
  a: 2
}
var counter = {
  var: 90212
}

if (window.matchMedia('(orientation: portrait)').matches) {
  console.log('PORTRAIT')
  tl_P
    .set(banner, {
      opacity: 0
    })
    .set(scene1, {
      opacity: 0,
      scale: 1
    })
    .set(scene2, {
      opacity: 0,
      yPercent: 0,
      scale: 1.2
    })
    .set(scene3, {
      opacity: 0,
      yPercent: 120,
      scale: 0.6
    })
    .set(photoFader, {
      opacity: 0
    })
    .set(photoFlash, {
      opacity: 0
    })
    .set(crosshair, {
      opacity: 0,
      scale: 2
    })
    .set(photo, {
      yPercent: 0,
      webkitFilter: 'blur(' + blurElement.a + 'px)',
      filter: 'blur(' + blurElement.a + 'px)'
    })
    .set(whiteBlock, {
      yPercent: 0
    })
    .set(message0, {
      yPercent: -100
    })
    .set(messageBlock, {
      yPercent: 100
    })
    .set(packshotText, {
      opacity: 0,
      yPercent: -120
    })
    .set(btnAppstore, {
      opacity: 0,
      yPercent: 30,
      scale: 0.6
    })
    .set(btnGoogleplay, {
      opacity: 0,
      yPercent: 30,
      scale: 0.6
    })

    .to(banner, 0.6, {
      opacity: 1
    }, startTime)
    .to(scene1, 0.1, {
      opacity: 1
    }, startTime)
    .to(shotButton, 0.1, {
      scale: 0.9,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(shotButton, 0.1, {
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.1)
    .to(crosshair, 1, {
      scale: 1,
      opacity: 1,
      ease: Back.easeOut.config(1.7)
    }, startTime += 0.1)
    .to(blurElement, 1, {
      a: 0,
      onUpdate: applyBlur
    }, startTime += 0.1)

    .to(photoFlash, 0.3, {
      opacity: 1
    }, startTime += 1)
    .to(photoFlash, 0.3, {
      opacity: 0
    }, startTime += 0.1)

    .to(crosshair, 0.3, {
      opacity: 0
    }, startTime += 0.6)

    .to(crosshair, 0.3, {
      opacity: 0
    }, startTime += 0.6)

    .to(whiteBlock, 0.6, {
      yPercent: 150,
      ease: Power1.easeOut
    }, startTime)
    .to(message0, 0.6, {
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime)
    .to(photo, 0.4, {
      yPercent: 5,
      ease: Power1.easeOut
    }, startTime += 0.3)

    .to(messageBlock, 0.4, {
      yPercent: 74,
      ease: Power1.easeOut
    }, startTime += 1.3)

    .to(photo, 0.4, {
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime)
    .to(message0, 0.8, {
      yPercent: -100,
      ease: Power1.easeOut
    }, startTime += 0.3)

    .to(messageBlock, 0.4, {
      yPercent: 50,
      ease: Power1.easeOut
    }, startTime += 1.3)
    .to(photo, 0.4, {
      yPercent: -5,
      ease: Power1.easeOut
    }, startTime)
    .to(counter, 0.5, {
      var: 90736,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(counter.var))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(messageBlock, 0.4, {
      yPercent: 25,
      ease: Power1.easeOut
    }, startTime += 1.3)
    .to(photo, 0.4, {
      yPercent: -10,
      ease: Power1.easeOut
    }, startTime)
    .to(counter, 0.5, {
      var: 90959,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(counter.var))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(messageBlock, 0.4, {
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime += 1.3)
    .to(photo, 0.4, {
      yPercent: -15,
      ease: Power1.easeOut
    }, startTime)
    .to(counter, 0.5, {
      var: 91256,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(counter.var))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(scene1, 0.5, {
      opacity: 0,
      scale: 0.5,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(scene2, 0.6, {
      opacity: 1,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.3)

    .to(scene2, 0.8, {
      opacity: 0,
      yPercent: 120,
      scale: 0.6,
      ease: Power1.easeOut
    }, startTime += 1.5)

    .to(scene3, 0.8, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.1)
    .to(btnAppstore, 0.3, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(btnGoogleplay, 0.3, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.3)
    .to(packshotText, 0.3, {
      opacity: 1,
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime += 0.6)
}
if (window.matchMedia('(orientation: landscape)').matches) {
  console.log('LANSCAPE')
  tl_P
    .set(banner, {
      opacity: 0
    })
    .set(scene1, {
      opacity: 0,
      scale: 1
    })
    .set(scene2, {
      opacity: 0,
      yPercent: 0,
      scale: 1.2
    })
    .set(scene3, {
      opacity: 0,
      yPercent: 120,
      scale: 0.6
    })
    .set(photoFader, {
      opacity: 0
    })
    .set(photoFlash, {
      opacity: 0
    })
    .set(crosshair, {
      opacity: 0,
      scale: 2
    })
    .set(photo, {
      yPercent: 0,
      webkitFilter: 'blur(' + blurElement.a + 'px)',
      filter: 'blur(' + blurElement.a + 'px)'
    })
    .set(whiteBlock, {
      yPercent: 0
    })
    .set(message0, {
      yPercent: -100
    })
    .set(message1, {
      opacity: 0
    })
    .set(message2, {
      opacity: 0
    })
    .set(message3, {
      opacity: 0
    })
    .set(messageBlock, {
      opacity: 0
    })
    .set(packshotText, {
      opacity: 0,
      yPercent: -120
    })
    .set(btnAppstore, {
      opacity: 0,
      yPercent: 30,
      scale: 0.6
    })
    .set(btnGoogleplay, {
      opacity: 0,
      yPercent: 30,
      scale: 0.6
    })

    .to(banner, 0.6, {
      opacity: 1
    }, startTime)
    .to(scene1, 0.1, {
      opacity: 1
    }, startTime)
    .to(shotButton, 0.1, {
      scale: 0.9,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(shotButton, 0.1, {
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.1)
    .to(crosshair, 1, {
      scale: 1,
      opacity: 1,
      ease: Back.easeOut.config(1.7)
    }, startTime += 0.1)
    .to(blurElement, 1, {
      a: 0,
      onUpdate: applyBlur
    }, startTime += 0.1)

    .to(photoFlash, 0.3, {
      opacity: 1
    }, startTime += 1)
    .to(photoFlash, 0.3, {
      opacity: 0
    }, startTime += 0.1)

    .to(crosshair, 0.3, {
      opacity: 0
    }, startTime += 0.6)

    .to(crosshair, 0.3, {
      opacity: 0
    }, startTime += 0.6)

    .to(whiteBlock, 0.6, {
      opacity: 0,
      ease: Power1.easeOut
    }, startTime)
    .to(message0, 0.6, {
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime)

    .to(messageBlock, 0.4, {
      opacity: 1,
      ease: Power1.easeOut
    }, startTime += 1.3)

    .to(message1, 0.4, {
      opacity: 1
    }, startTime += 1.3)

    .to(counter, 0.5, {
      var: 90736,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(Number(counter.var)))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(message2, 0.4, {
      opacity: 1
    }, startTime += 1.3)
    .to(counter, 0.5, {
      var: 90959,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(Number(counter.var)))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(message3, 0.4, {
      opacity: 1
    }, startTime += 1.3)

    .to(counter, 0.5, {
      var: 91256,
      onUpdate: function () {
        numbers.innerHTML = (Math.ceil(Number(counter.var)))
      },
      ease: Circ.easeOut
    }, startTime)

    .to(scene1, 0.5, {
      opacity: 0,
      scale: 0.5,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(scene2, 0.6, {
      opacity: 1,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.3)

    .to(scene2, 0.8, {
      opacity: 0,
      yPercent: 120,
      scale: 0.6,
      ease: Power1.easeOut
    }, startTime += 1.5)

    .to(scene3, 0.8, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.1)
    .to(btnAppstore, 0.3, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 1)
    .to(btnGoogleplay, 0.3, {
      opacity: 1,
      yPercent: 0,
      scale: 1,
      ease: Power1.easeOut
    }, startTime += 0.3)
    .to(packshotText, 0.3, {
      opacity: 1,
      yPercent: 0,
      ease: Power1.easeOut
    }, startTime += 0.6)
}

window.addEventListener('orientationchange', function () {
  if (window.matchMedia('(orientation: portrait)').matches) {
    console.log('PORTRAIT')
  }
  if (window.matchMedia('(orientation: landscape)').matches) {
    console.log('LANSCAPE')
  }
}, false)
