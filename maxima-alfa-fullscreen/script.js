// window.screen
// window.innerWidth
// ================================
var TweenMax = window.TweenMax
var Linear = window.Linear
var Quad = window.Quad
var Power4 = window.Power4
var Power2 = window.Power4
var Power0 = window.Power0
var o

// document.addEventListener('DOMContentLoaded', init)
window.addEventListener('orientationchange', function () {
  setTimeout(function () {
    init()
  }, 600)
})
// window.addEventListener('resize', init)

var tl1 = new TimelineLite({
  delay: 0,
  repeat: 0,
  repeatDelay: 0,
  paused: true
})
var tl2 = new TimelineLite({
  delay: 0,
  repeat: 0,
  repeatDelay: 0,
  paused: true
})

function init () {
  TweenMax.fromTo('.wrapper', 0.6, {
    opacity: 0
  }, {
    opacity: 1
  }, 0)
  // tl1.pause(0, true)
  // tl2.pause(0, true)
  tl1.clear()
  tl2.clear()

  tl1 = new TimelineLite({
    delay: 0,
    repeat: 0,
    repeatDelay: 0,
    paused: true
  })
  tl2 = new TimelineLite({
    delay: 0,
    repeat: 0,
    repeatDelay: 0,
    paused: true
  })

  getOrientation()

  startTime = 0
  startTime1 = 0

  if (o === 'Portrait') {
    // tl2.pause(0, true)

    var letterLine1 = ['.letter-p-1', '.letter-p-2', '.letter-p-3', '.letter-p-4']
    var letterLine2 = ['.letter-p-5', '.letter-p-6', '.letter-p-7', '.letter-p-8', '.letter-p-9', '.letter-p-10', '.letter-p-11', '.letter-p-12']
    var letterLine3 = ['.letter-p-13', '.letter-p-14', '.letter-p-15', '.letter-p-16', '.letter-p-17', '.letter-p-18', '.letter-p-19', '.letter-p-20', '.letter-p-21', '.letter-p-22', '.letter-p-23']

    tl1
      .from('.logo-line', 0.8, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime += 0.3)
      .staggerFrom(letterLine1, 1, {
        xPercent: 100,
        rotationY: 90,
        ease: Power2.easeOut
      }, 0.3, startTime += 0.3)
      .staggerFrom(letterLine2, 0.7, {
        xPercent: 100,
        rotationY: 90,
        ease: Power2.easeOut
      }, 0.3, startTime += 0.8)
      .staggerFrom(letterLine3, 0.8, {
        xPercent: 100,
        rotationY: 90,
        ease: Power2.easeOut
      }, 0.2, startTime += 2.6)
      // .to('.logo-line', 1, {
      //   xPercent: -50,
      //   ease: Power0.easeNone
      // }, startTime)
      .staggerFrom(['.small-text-1', '.small-text-2'], 1, {
        yPercent: 40,
        opacity: 0,
        ease: Power2.easeOut
      }, 0.4, startTime += 1.8)
      .to('.letters-block', 0.5, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime += 4)
      .from('.packshot-legal', 0.3, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime += 0.5)
      .staggerFrom(['.packshot-logo', '.packshot-url', '.packshot-button'], 0.6, {
        opacity: 0,
        ease: Power0.easeNone
      }, 0.6, startTime += 0.6)

    tl1.play()
  }
  if (o === 'Landscape') {
    // tl1.pause(0, true)

    var letterLine1 = ['.letter-p-1', '.letter-p-2', '.letter-p-3', '.letter-p-4', '.letter-p-5', '.letter-p-6', '.letter-p-7', '.letter-p-8', '.letter-p-9', '.letter-p-10', '.letter-p-11', '.letter-p-12']
    var letterLine2 = ['.letter-p-13', '.letter-p-14', '.letter-p-15', '.letter-p-16', '.letter-p-17', '.letter-p-18', '.letter-p-19', '.letter-p-20', '.letter-p-21', '.letter-p-22', '.letter-p-23']

    tl2
      .from('.logo-line', 0.8, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime1 += 0.3)
      .staggerFrom(letterLine1, 0.7, {
        xPercent: 100,
        rotationY: 90,
        ease: Power2.easeOut
      }, 0.2, startTime1 += 0.3)
      .staggerFrom(letterLine2, 0.8, {
        xPercent: 100,
        rotationY: 90,
        ease: Power2.easeOut
      }, 0.2, startTime1 += 2.6)
      // .to('.logo-line', 1, {
      //   xPercent: -50,
      //   ease: Power0.easeNone
      // }, startTime1)
      .staggerFrom(['.small-text-1', '.small-text-2'], 1, {
        yPercent: 40,
        opacity: 0,
        ease: Power2.easeOut
      }, 0.4, startTime1 += 1.8)
      .to('.letters-block', 0.5, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime1 += 4)
      .from('.packshot-legal', 0.3, {
        opacity: 0,
        ease: Power0.easeNone
      }, startTime1 += 0.5)
      .staggerFrom(['.packshot-logo', '.packshot-url', '.packshot-button'], 0.6, {
        opacity: 0,
        ease: Power0.easeNone
      }, 0.6, startTime1 += 0.6)

    tl2.play()
  }
}

function getOrientation () {
  o = wrapper.clientWidth > wrapper.clientHeight ? 'Landscape' : 'Portrait'
}

init()
