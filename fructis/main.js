(function () {
  var lastTime = 0
  var vendors = ['ms', 'moz', 'webkit', 'o']
  for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
    window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame']
    window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] ||
      window[vendors[x] + 'CancelRequestAnimationFrame']
  }

  if (!window.requestAnimationFrame) {
    window.requestAnimationFrame = function (callback, element) {
      var currTime = new Date().getTime()
      var timeToCall = Math.max(0, 16 - (currTime - lastTime))
      var id = window.setTimeout(function () {
        callback(currTime + timeToCall)
      },
        timeToCall)
      lastTime = currTime + timeToCall
      return id
    }
  }

  if (!window.cancelAnimationFrame) {
    window.cancelAnimationFrame = function (id) {
      clearTimeout(id)
    }
  }
}());

(function () {
  var dragZone = document.getElementById('drag-zone')
  var prodSlider = document.getElementById('prod-slider')
  var volOn = document.getElementById('vol-on')

  function getCoords (elem) {
    return {
      top: parseFloat(elem.offsetTop),
      left: parseFloat(elem.offsetLeft)
    }
  }

  function unFocus () {
    if (document.selection) {
      document.selection.empty()
    } else {
      window.getSelection().removeAllRanges()
    }
  }

  var prodSliderMin = getCoords(prodSlider).left
  var prodSliderMax = prodSliderMin + 155

  prodSlider.style.left = -50 + 'px'
  volOn.style.width = 0 + 'px'

  var range = prodSliderMax - prodSliderMin
  var oneStripe = range / 7

  dragZone.onmousedown = function (e) {
    if (isBlinkEnd && canClick) {
      if (!isUserAction) {
        autoRunSlider.pause()
        autoRunBars.pause()
        isUserAction = true
      }

      var coords = getCoords(prodSlider)

      var shiftX = e.pageX - coords.left
      var shiftY = e.pageY - coords.top

      moveAt(e)

      prodSlider.style.zIndex = 1000

      function moveAt (e) {
        volOn.style.width = e.pageX - shiftX + 50 + 'px'
        prodSlider.style.left = e.pageX - shiftX + 'px'
        prodSlider.style.top = coords.top + 'px'
      }

      document.onmousemove = function (e) {
        unFocus()

        if (getCoords(prodSlider).left >= prodSliderMin && getCoords(prodSlider).left <= prodSliderMax) {
          moveAt(e)

          iconFadeIn()
        }

        if (getCoords(prodSlider).left < prodSliderMin) {
          prodSlider.style.left = prodSliderMin + 'px'
          unFocus()

          document.onmousemove = null
          prodSlider.onmouseup = null
        }
        if (getCoords(prodSlider).left > prodSliderMax) {
          prodSlider.style.left = prodSliderMax + 'px'
          unFocus()

          document.onmousemove = null
          prodSlider.onmouseup = null
          clearTimeout(timerAutoPlay)
          canClick = false
          setTimeout(function () {
            hideText2.play()
            showText3.play()
            moveSliderAndIcons.play()
            showNext()
          }, 1000)
        }
      }
    }

    timerAutoPlay = 1

    dragZone.onmouseup = function () {
      unFocus()

      document.onmousemove = null
      prodSlider.onmouseup = null

      if (runAutoOnce) {
        timerAutoPlay = setTimeout(runAfter, 4000)
        runAutoOnce = false
      }
    }
  }

  dragZone.ondragstart = function () {
    return false
  }

  function runAfter () {
    var autoRunSlider = anime({
      targets: '#prod-slider',
      left: 106,
      duration: 600,
      delay: 0,
      easing: 'linear'
    })
    var autoRunBars = anime({
      targets: '#vol-on',
      width: 170,
      duration: 600,
      delay: 0,
      easing: 'linear',
      update: function () {
        aPlay()
      }
    })
  }

  function iconFadeIn () {
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 1) {
      if (!isIcon1Shown) {
        showIcon1.play()
        isIcon1Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 2) {
      if (!isIcon2Shown) {
        showIcon2.play()
        isIcon2Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 3) {
      if (!isIcon3Shown) {
        showIcon3.play()
        isIcon3Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 4) {
      if (!isIcon4Shown) {
        showIcon4.play()
        isIcon4Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 5) {
      if (!isIcon5Shown) {
        showIcon5.play()
        isIcon5Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 6) {
      if (!isIcon6Shown) {
        showIcon6.play()
        isIcon6Shown = true
      }
    }
    if (getCoords(prodSlider).left > prodSliderMin + oneStripe * 7) {
      if (!isIcon7Shown) {
        showIcon7.play()
        isIcon7Shown = true
      }
    }
  }

  function aPlay () {
    iconFadeIn()
    if (getCoords(prodSlider).left > 105) {
      prodSlider.style.left = 106 + 'px'
      unFocus()

      document.onmousemove = null
      prodSlider.onmouseup = null

      setTimeout(function () {
        hideText2.play()
        showText3.play()
        moveSliderAndIcons.play()
        showNext()
      }, 1000)
    }
  }
  // Animations
  var scene1 = document.getElementById('scene1')
  var text1 = document.getElementById('text1')
  var text2 = document.getElementById('text2')
  var sliderBlock = document.getElementById('slider-block')
  var scene3 = document.getElementById('scene3')
  var scene4 = document.getElementById('scene3')

  var smallIcons = document.getElementById('small-icons')
  var iconBall = document.getElementById('icon-ball')
  var iconBlesk = document.getElementById('icon-blesk')
  var iconBlesk2 = document.getElementById('icon-blesk2')
  var iconButterfly = document.getElementById('icon-butterfly')
  var iconCam = document.getElementById('icon-cam')
  var iconCycle = document.getElementById('icon-cycle')
  var iconDrink = document.getElementById('icon-drink')

  var girl = document.getElementById('girl')
  var text3 = document.getElementById('text3')
  var prod2 = document.getElementById('prod2')

  var textLogo = document.getElementById('text-logo')
  var textHashtag = document.getElementById('text-hashtag')
  var prodAll = document.getElementById('prod-all')
  var button = document.getElementById('button')

  var slomoBlock = document.getElementById('slomo-block')

  var isIcon1Shown
  var isIcon2Shown
  var isIcon3Shown
  var isIcon4Shown
  var isIcon5Shown
  var isIcon6Shown
  var isIcon7Shown
  var isUserAction
  var runAutoOnce

  var isBlinkEnd

  function run () {
    isBlinkEnd = false

    canClick = true

    isUserAction = false
    runAutoOnce = true

    isIcon1Shown = false
    isIcon2Shown = false
    isIcon3Shown = false
    isIcon4Shown = false
    isIcon5Shown = false
    isIcon6Shown = false
    isIcon7Shown = false

    // mainPl.style.opacity = 0;

    smallIcons.style.opacity = 0
    scene1.style.opacity = 0
    text1.style.left = 15 + 'px'
    text2.style.left = 300 + 'px'
    text3.style.left = 300 + 'px'
    sliderBlock.style.opacity = 0

    iconBall.style.opacity = 0.1
    iconBlesk.style.opacity = 0.8
    iconBlesk2.style.opacity = 0.7
    iconButterfly.style.opacity = 0.2
    iconCam.style.opacity = 0.9
    iconCycle.style.opacity = 0.1
    iconDrink.style.opacity = 0.6

    girl.style.left = -300 + 'px'
    text3.style.left = 300 + 'px'
    prod2.style.left = -100 + 'px'

    textLogo.style.left = 300 + 'px'
    textHashtag.style.left = 300 + 'px'
    prodAll.style.left = 300 + 'px'
    button.style.left = 300 + 'px'

    slomoBlock.style.left = 0
  }

  run()

  var autoRunSlider = anime({
    targets: '#prod-slider',
    left: 106,
    duration: 1600,
    delay: 7800,
    easing: 'linear',
    autoplay: false
  })
  var autoRunBars = anime({
    targets: '#vol-on',
    width: 170,
    duration: 1600,
    delay: 2400,
    easing: 'linear',
    autoplay: false,
    update: function () {
      aPlay()
    }
  })

  var showFirstScene = anime({
    targets: '#scene1',
    opacity: 1,
    duration: 400,
    easing: 'linear',
    autoplay: false,
    complete: function () {
      anime({
        targets: '#scene1',
        opacity: 0,
        duration: 400,
        delay: 2000,
        easing: 'linear'
      })
    }
  })

  var hideText1 = anime({
    targets: '#text1',
    translateX: -240,
    duration: 600,
    delay: 2000,
    easing: 'easeInQuint',
    autoplay: false
  })

  var showText2 = anime({
    targets: '#text2',
    left: 15,
    duration: 600,
    delay: 2400,
    easing: 'easeOutQuint',
    autoplay: false,
    complete: function () {
      anime({
        targets: '#small-icons',
        opacity: 1,
        duration: 400,
        easing: 'linear'
      })
    }
  })

  var hideText2 = anime({
    targets: '#text2',
    translateX: -240,
    duration: 600,
    delay: 600,
    easing: 'easeInQuint',
    autoplay: false
  })

  var showText3 = anime({
    targets: '#text3',
    left: 15,
    duration: 600,
    delay: 1000,
    easing: 'easeOutQuint',
    autoplay: false
  })

  var moveSliderAndIcons = anime({
    targets: '#small-icons, #slider-block',
    translateX: 320,
    duration: 1400,
    delay: 0,
    easing: 'easeOutQuint',
    autoplay: false,
    complete: function () {

    }
  })

  var showNext = function () {
    anime({
      targets: '#slomo-block',
      translateX: 100,
      duration: 5500,
      delay: 900,
      easing: 'linear'
    })
    anime({
      targets: '#girl',
      translateX: 320,
      duration: 1800,
      delay: 0,
      easing: 'easeOutQuint',
      autoplay: true
    })
    anime({
      targets: '#prod2',
      left: -40,
      duration: 600,
      delay: 1000,
      easing: 'easeOutQuad',
      autoplay: true,
      complete: function () {
        anime({
          targets: '#prod2',
          translateX: 0,
          duration: 3000,
          delay: 0,
          easing: 'linear',
          complete: function () {
            anime({
              targets: '#girl',
              translateX: 580,
              duration: 600,
              delay: 0,
              easing: 'easeInQuint'
            })
            anime({
              targets: '#prod2',
              translateX: 265,
              duration: 650,
              delay: 0,
              easing: 'easeInQuint'
            })
            anime({
              targets: '#text3',
              translateX: -240,
              duration: 400,
              easing: 'easeInQuint'
            })
            anime({
              targets: '#prod-all',
              left: 5,
              duration: 1000,
              delay: 500,
              easing: 'easeOutQuint'
            })
            anime({
              targets: '#text-logo',
              left: 15,
              duration: 1000,
              delay: 800,
              easing: 'easeOutQuint'
            })
            anime({
              targets: '#text-hashtag',
              left: 20,
              duration: 1000,
              delay: 800,
              easing: 'easeOutQuint'
            })
            anime({
              targets: '#button',
              left: 8,
              duration: 1000,
              delay: 1000,
              easing: 'easeOutQuint',
              complete: function () {
                anime({
                  targets: '.scene4',
                  opacity: 0,
                  duration: 600,
                  delay: 2000,
                  easing: 'linear',
                  complete: function () {
                    // mainPl.style.display = "block";
                    // location.reload();

                    showFirstScene.play()
                    hideText1.play()
                    showText2.play()
                    showSliderBlock.play()
                  }
                })
              }
            })
          }
        })
      }
    })
  }
  var showSliderBlock = anime({
    targets: '#slider-block',
    opacity: 1,
    duration: 600,
    delay: 2600,
    easing: 'easeOutQuad',
    autoplay: false,
    complete: function () {
      autoRunBars.pause()
      // mainPl.style.display = "none";
      anime({
        targets: '.arrow-a',
        opacity: 1,
        duration: 200,
        easing: 'linear'
      })
      anime({
        targets: '.arrow-line',
        opacity: 1,
        duration: 200,
        easing: 'linear'
      })
      anime({
        targets: '.arrow-a',
        left: 100,
        duration: 580,
        easing: 'easeOutQuad',
        complete: function () {
          anime({
            targets: '.arrow-a',
            opacity: 0,
            delay: 350,
            duration: 140,
            easing: 'linear',
            complete: function () {
              document.getElementsByClassName('arrow-a')[0].style.left = 0 + 'px'
              document.getElementsByClassName('arrow-a')[0].style.opacity = 1
              anime({
                targets: '.arrow-a',
                left: 100,
                duration: 580,
                delay: 100,
                easing: 'easeOutQuad',
                complete: function () {
                  anime({
                    targets: '.arrow-a',
                    opacity: 0,
                    delay: 330,
                    duration: 100,
                    easing: 'linear',
                    complete: function () {

                    }
                  })
                }
              })
            }
          })
        }
      })

      anime({
        targets: '.arrow-line',
        left: 0,
        duration: 600,
        easing: 'easeOutQuad',
        complete: function () {
          anime({
            targets: '.arrow-line',
            left: 112,
            duration: 500,
            easing: 'easeOutQuad',
            complete: function () {
              anime({
                targets: '.arrow-line',
                opacity: 0,
                duration: 100,
                easing: 'linear',
                complete: function () {
                  document.getElementsByClassName('arrow-line')[0].style.left = -116 + 'px'
                  document.getElementsByClassName('arrow-line')[0].style.opacity = 1
                  anime({
                    targets: '.arrow-line',
                    left: 0,
                    duration: 600,
                    easing: 'easeOutQuad',
                    complete: function () {
                      anime({
                        targets: '.arrow-line',
                        left: 112,
                        duration: 500,
                        easing: 'easeOutQuad',
                        complete: function () {
                          anime({
                            targets: '.arrow-line',
                            opacity: 0,
                            duration: 100,
                            easing: 'linear',
                            complete: function () {

                            }
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
        }
      })
      anime({
        targets: '#vol-on-a',
        width: 170,
        duration: 600,
        easing: 'easeOutQuad',
        complete: function () {
          anime({
            targets: '#vol-on-a',
            width: 0,
            duration: 500,
            easing: 'easeOutQuad',
            complete: function () {
              anime({
                targets: '#vol-on-a',
                width: 170,
                duration: 600,
                easing: 'easeOutQuad',
                complete: function () {
                  anime({
                    targets: '#vol-on-a',
                    width: 0,
                    duration: 500,
                    easing: 'easeOutQuad',
                    complete: function () {
                      isBlinkEnd = true
                      autoRunBars.play()
                    }
                  })
                }
              })
            }
          })
        }
      })
    }
  })

  var showIcon1 = anime({
    targets: '#icon-ball',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon2 = anime({
    targets: '#icon-blesk',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon3 = anime({
    targets: '#icon-blesk2',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon4 = anime({
    targets: '#icon-butterfly',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon5 = anime({
    targets: '#icon-cam',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon6 = anime({
    targets: '#icon-cycle',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })
  var showIcon7 = anime({
    targets: '#icon-drink',
    opacity: 1,
    duration: 500,
    easing: 'linear',
    autoplay: false
  })

  showFirstScene.play()
  hideText1.play()
  showText2.play()
  showSliderBlock.play()
  autoRunSlider.play()
})()
